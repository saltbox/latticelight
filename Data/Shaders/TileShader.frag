#version 300 es

uniform sampler2D texture1;

in mediump vec3 color;
in mediump vec3 bg_color;
in mediump vec2 texCoord0;

out mediump vec4 col;

void main(void){
	col = texture2D(texture1, texCoord0) * vec4(color, 1.0);
	col.xyz = mix(bg_color, col.xyz, col.a);
}
