#version 300 es

in mediump vec3 in_position;
in mediump vec3 in_color;
in mediump vec2 in_texCoord0;
in mediump vec3 in_bg_color;

out mediump vec3 color;
out mediump vec3 bg_color;
out mediump vec2 texCoord0;

uniform mediump mat4 transform;

void main(void){
	gl_Position = transform * vec4(in_position, 1.0);
	color = in_color;
	bg_color = in_bg_color;
	texCoord0 = in_texCoord0;
}
