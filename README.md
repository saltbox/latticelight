# LatticeLight #

LatticeLight is a small game engine built on top of LWJGL3 for building ASCII style games.

### How do I get set up? ###

Right now setting up takes a bit more work than it hopefully will in the future.

###### Eclipse ######
* Git clone LatticeLight into your Eclipse workspace.
* Open Eclipse and import the LatticeLight folder as a Maven project.
* Create your own new project as a Maven project.
* Copy LatticeLight's dependencies from pom.xml into your project's pom.xml
* Open your project's settings and add the LatticeLight project under 'Build Path'->'Projects'
* Copy the Data folder from LatticeLight's root directory into your projects root directory

* Create a Class that extends com.saltboxgames.latticelight.game
* Create an instance of your class and call begin to start your game..

### Contribute ###
If you have an idea or feel like something necessary is missing from the project, 
feel free to create a fork and implement your idea, 
when it's done you can make a pull request and we'll merge your work in, if it fits in with LatticeLight.

or you can add a proposal to the Issue tracker and someone else might get to it.

### Nothing Works? ###

If you have any problems check the issue tracker on your left. if your problem isn't there feel free to post a new issue.