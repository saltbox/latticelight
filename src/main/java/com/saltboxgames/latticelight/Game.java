package com.saltboxgames.latticelight;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.system.MemoryUtil.NULL;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import org.joml.Vector3f;
import org.joml.Vector2f;
import org.joml.Vector2i;
import org.lwjgl.glfw.GLFWCharCallback;
import org.lwjgl.glfw.GLFWKeyCallback;
import org.lwjgl.glfw.GLFWVidMode;
import org.lwjgl.opengl.GL;

import com.saltboxgames.latticelight.input.*;
import com.saltboxgames.latticelight.logging.*;
import com.saltboxgames.latticelight.rendering.*;
import com.saltboxgames.latticelight.utils.*;

public abstract class Game 
{
	// Gameloop & Status
	private boolean isRunning;
	private boolean isHeadless;
	private int currentFPS, tickRate = 60;
	
	//OpenGL / Window
	private Vector2i size;
	private int width, height;
	private float fontSize;
	private long window;
	
	//Camera & Rendering
	private Camera mainCamera;
	private SpriteBatch spriteBatch;
	private boolean hasContext;
	
	//--Shaders & Textures
	Shader defaultShader;
	Texture defaultTexture;
	
	//--Engine Tools
	private SpritesheetTester spritesheetTester;
	private FPSCounter fpsCounter;

	//Logging
	private boolean logEnabled;
	private int consoleKeyCode = GLFW_KEY_GRAVE_ACCENT;
	private int consoleAltKeyCode = GLFW_KEY_LEFT_SHIFT;
	private GameConsole gameConsole;
	
	//Input
	private GLFWKeyCallback keyCallback;
	private GLFWCharCallback charCallback;
	private MouseInput mouseInput;
	
	//Game Info
	private String title;
	
	//Commands
	private HashMap<String, Command> commands;
	
	public Game(String title, Vector2i size, int fontSize)
	{
		this.title = title;
		this.size = size;
		this.fontSize = fontSize;
		this.width = (size.x ) * fontSize;
		//this.width = (size.x - 1) * fontSize; //<-- Really confused about needing the minus 1 for things to be square (Removed? might need to be replaced)
		this.height = size.y * fontSize;
		
		commands = new HashMap<String, Command>();
		initDefaultCommands();
	}
	
	public Game(String title, Vector2i size, int fontSize, boolean headlessMode)
	{
		this(title, size, fontSize);
		this.isHeadless = headlessMode;
	}
	
	public void End()
	{
		Stop();
		isRunning = false;	
	}
	
	public void Begin()
	{
		init();
		if(!isHeadless)
		{
			initGl();	
		}
		
		hasContext = (window != 0);
		
		if(hasContext)
		{
			//Init Engine Tools
			  
			//Logger.
			gameConsole = new GameConsole(this, new Vector2f(), new Vector2i(size.x, (size.y / 2) + 2));
			KeyboardInput.registerKeyCallBack(gameConsole);
			CharCallbackHandler.registerCallBack(gameConsole);
			Logger.registerLog(gameConsole);
			
			//Sprite Tester
			spritesheetTester = new SpritesheetTester(fontSize);
			
			//FPS
			fpsCounter = new FPSCounter(new Vector2f(size.x - 9, 0), 95f, this);
			
		}
		else 
		{
			Logger.registerLog(new ConsoleLogger());
		}
		
  		//INIT GAME HERE:
		Logger.writeLine(title + ": ");
		
		Start();
		
		isRunning = true;
		gameLoop();
	}
	
	public void setTickRate(int rate) 
	{
		tickRate = rate;
		System.out.println(rate);
	}
	
	public abstract void Start();
	public abstract void Stop();
	
	public abstract void LoadContent();
	
	public abstract void Update(GameTime gameTime);
	public abstract void Draw(SpriteBatch spriteBatch);
	
	// Internal Engine Code.
	private void init()
	{
		Logger.writeLine("Staring LatticeLight Game... ");
		this.isRunning = true;
	}
	
	private void initGl()
	{
		// Init Window
		if(glfwInit() != GL_TRUE)
		{
			//FAILED TO INTI GLEW!
			Logger.writeLine("GLEW initialization failed", Logger.LOG_ERROR);
			return;
		}
		
		glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
		
		window = glfwCreateWindow(width, height, title, NULL, NULL);
		if(window == NULL)
		{
			Logger.writeLine("Window creation failed", Logger.LOG_ERROR);
			return;
		}
				
		//Set Input CallBacks
		keyCallback = KeyboardInput.getInstance();
		glfwSetKeyCallback(window, keyCallback);
		
		charCallback = CharCallbackHandler.getInstance();
		glfwSetCharCallback(window, charCallback);
		
		mouseInput = MouseInput.getInstance();
		mouseInput.registerCallBacks(window);
		
		
		GLFWVidMode vidMode = glfwGetVideoMode(glfwGetPrimaryMonitor());
		glfwSetWindowPos( 
				window,
	            (vidMode.width() - width) / 2,
	            (vidMode.height() - height) / 2
			);
		
		glfwMakeContextCurrent(window);
		
		glfwShowWindow(window);
		
		GL.createCapabilities();
		Logger.writeLine("OpenGL: " + glGetString(GL_VERSION), Logger.LOG_DEBUG);
              
        glEnable(GL_TEXTURE_2D);
        glEnable(GL_DEPTH_TEST);
              
        //glClearColor(0.392f, 0.584f, 0.929f, 1.0f); //<-- Corn Flower Blue	
        glClearColor(0, 0, 0, 1);
 
        // Load default Shader & Texture
        defaultShader = new Shader("Data/Shaders/TileShader.vert", "Data/Shaders/TileShader.frag");
		defaultTexture = new Texture("Data/ASCII.png");
          
        //INIT SPRITEBATCH
      	spriteBatch = new SpriteBatch();
      	spriteBatch.setShader(defaultShader);
      	spriteBatch.setTexture(defaultTexture);
        
  		//init default Camera
  		mainCamera = new Camera(new Vector3f(0), width, height, (1.0f / fontSize));
  		Camera.Main = mainCamera;
	}
	
	private void doDraw()
	{
		if(hasContext)
		{
			glfwSwapBuffers(window);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			
			// Draw Engine Tools
			if(TOOL_PARAMS[TOOL_GAME_CONSOLE] > 0)
			{
				gameConsole.Draw(spriteBatch);
			}
			
			if (TOOL_PARAMS[TOOL_SPRITE_SHEET] > 0) 
			{
				spritesheetTester.Draw(spriteBatch);
			}
			
			if(TOOL_PARAMS[TOOL_FPS_COUNTER] > 0)
			{
				fpsCounter.Draw(spriteBatch);
			}
			
			//RENDER GAME HERE:
			Draw(spriteBatch);	
		}
	}

	private void doUpdate(GameTime gameTime)
	{
		if(hasContext)
		{
			KeyboardInput.Update();
			MouseInput.Update();
			glfwPollEvents(); // <-- Updates window events.	
		}
			
		if(KeyboardInput.isKeyDown(consoleKeyCode) && !KeyboardInput.isKeyDown(consoleKeyCode, true))
		{

			if(TOOL_PARAMS[TOOL_GAME_CONSOLE] == 0)
			{

				if(KeyboardInput.isKeyDown(consoleAltKeyCode))
				{
					TOOL_PARAMS[TOOL_GAME_CONSOLE] = 2;
				}
				else
				{
					TOOL_PARAMS[TOOL_GAME_CONSOLE] = 1;
				}
			}
			else 
			{
				TOOL_PARAMS[TOOL_GAME_CONSOLE] = 0;
			}
		}
		
		
		// Update Engine Tools
		if(TOOL_PARAMS[TOOL_GAME_CONSOLE] > 0)
		{
			gameConsole.Update(gameTime);
		}
		
		if(TOOL_PARAMS[TOOL_SPRITE_SHEET] > 0) 
		{
			spritesheetTester.Update(gameTime);
		}
		
		if(TOOL_PARAMS[TOOL_FPS_COUNTER] > 0)
		{
			fpsCounter.Update(gameTime);
		}
		
		// UPDATE GAME HERE:
		Update(gameTime);
	}
	
	private void gameLoop()
	{
		GameTime gameTime = new GameTime();
		
		double deltaTime = 0;
		int frameCount = 0;
    	double fpsTime = 0;

    	sync();
        while (isRunning) 
        {
        	if(window != 0)
    		{
	        	if(glfwWindowShouldClose(window) != GLFW_FALSE)
	        	{
	        		Stop();
	        		isRunning = false;
	        	}
    		}
        	
        	sync();
        	
        	deltaTime = updateLength / 1000000000.0f;
			gameTime.setDeltaTime(deltaTime);
			
			frameCount ++;
			fpsTime += deltaTime;
			
			if(fpsTime >= 1)
			{
				currentFPS = frameCount;
				fpsTime -= 1;
				frameCount = 0;
			}

			doUpdate(gameTime);
			
			doDraw();		
        }
	}
	
	private long variableYieldTime, lastTime, updateLength;
	 /**
     * An accurate sync method that adapts automatically
     * to the system it runs on to provide reliable results.
     * 
     * @param fps The desired frame rate, in frames per second
     * @author kappa (On the LWJGL Forums) http://forum.lwjgl.org/index.php?topic=4452.0
     * Finley
     */
    private void sync() 
    {
    	if (tickRate <= 0) 
    	{
    		updateLength = System.nanoTime() - lastTime;
    		lastTime = System.nanoTime();
    		return;
		}
    	
          
        long sleepTime = 1000000000 / tickRate; // nanoseconds to sleep this frame
        // yieldTime + remainder micro & nano seconds if smaller than sleepTime
        long yieldTime = Math.min(sleepTime, variableYieldTime + sleepTime % (1000*1000));
        long overSleep = 0; // time the sync goes over by
          
        try 
        {
            while (true) 
            {
            	updateLength = System.nanoTime() - lastTime;
                  
                if (updateLength < sleepTime - yieldTime) 
                {
                    Thread.sleep(1);
                }else if (updateLength < sleepTime) 
                {
                    // burn the last few CPU cycles to ensure accuracy
                    Thread.yield();
                }else 
                {
                    overSleep = updateLength - sleepTime;
                    break; // exit while loop
                }
            }
        }
        catch (InterruptedException e) 
        {
            e.printStackTrace();
        }
        finally
        {
            lastTime = System.nanoTime() - Math.min(overSleep, sleepTime);
             
            // auto tune the time sync should yield
            if (overSleep > variableYieldTime) 
            {
                // increase by 200 microseconds (1/5 a ms)
                variableYieldTime = Math.min(variableYieldTime + 200*1000, sleepTime);
            }
            else if (overSleep < variableYieldTime - 200*1000) 
            {
                // decrease by 2 microseconds
                variableYieldTime = Math.max(variableYieldTime - 2*1000, 0);
            }
        }
    }
	
	// -- Commands
	public void registerCommand(String name, Command com)
	{
		if(!commands.containsKey(name.toLowerCase()))
		{
			commands.put(name.toLowerCase(), com);
		}
	}
	
	public boolean executeCommand(String name, String[] args) 
	{
		Command f = commands.get(name.toLowerCase());
		if(f != null)
		{
			f.run(args);
			return true;
		}
		return false;
	}
	
	public boolean executeCommand(String name, String arg0) 
	{
		return executeCommand(name, new String[]{ arg0 });
	}

	public boolean executeCommand(String name) 
	{
		return executeCommand(name, new String[0]);
	}
	
	private void initDefaultCommands()
	{
		registerCommand("ll.TickRate", (String[] args) -> 
		{
			if(args.length > 0)
			{
				if(Utilities.canParseInt(args[0]))
				{
					int val = Integer.parseInt(args[0]);
					if(val >= 0)
					{
						setTickRate(val);
					}
					else 
					{
						Logger.writeLine("FAILED: tick rate must be greater than or equal to 0");
					}
				}
				else 
				{
					Logger.writeLine("FAILED: tick rate must be an integer");
				}
			}
		});
		
		registerCommand("ll.spritepicker", (String[] args) -> 
		{
			if(args.length > 0)
			{
				if(args[0].equals("0"))
				{
					TOOL_PARAMS[TOOL_SPRITE_SHEET] = 0;
				}
				else if (args[0].equals("1")) 
				{
					TOOL_PARAMS[TOOL_SPRITE_SHEET] = 1;
				}
				else 
				{
					Logger.writeLine("Sprite Picker state can only be 0 or 1");
				}
			}
			else 
			{
				Logger.writeLine("Sprite Picker state can only be 0 or 1");
			}
		});
		
		registerCommand("ll.fps", (String[] args) -> 
		{
			if(args.length > 0)
			{
				if(args[0].equals("0"))
				{
					TOOL_PARAMS[TOOL_FPS_COUNTER] = 0;
				}
				else if (args[0].equals("1")) 
				{
					TOOL_PARAMS[TOOL_FPS_COUNTER] = 1;
				}
				else 
				{
					Logger.writeLine("FPS Counter state can only be 0 or 1");
				}
			}
			else 
			{
				Logger.writeLine("FPS Counter state can only be 0 or 1");
			}
		});
		
		registerCommand("ll.vsync", (String[] args) -> 
		{
			if(args.length > 0)
			{
				if(args[0].equals("0"))
				{
					TOOL_PARAMS[TOOL_VSYNC] = 0;
				}
				else if (args[0].equals("1")) 
				{
					TOOL_PARAMS[TOOL_VSYNC] = 1;
				}
				else 
				{
					Logger.writeLine("VSYNC state can only be 0 or 1");
				}
				
				glfwSwapInterval(TOOL_PARAMS[TOOL_VSYNC]);
			}
			else 
			{
				Logger.writeLine("VSYNC state can only be 0 or 1");
			}
		});
		
		registerCommand("ll.oscursor", (String[] args) -> 
		{
			if(args.length > 0)
			{
				if(args[0].equals("0"))
				{
					TOOL_PARAMS[TOOL_OS_CURSOR] = 0;
					glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
				}
				else if (args[0].equals("1")) 
				{
					TOOL_PARAMS[TOOL_OS_CURSOR] = 1;
					glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
				}
				else 
				{
					Logger.writeLine("OS Cursor state can only be 0 or 1");
				}
				
				glfwSwapInterval(TOOL_PARAMS[TOOL_OS_CURSOR]);
			}
			else 
			{
				Logger.writeLine("OS Cursor state can only be 0 or 1");
			}
		});
		
	}

	// -- Getters / Setters
	public void setHeadless(boolean isHeadless) 
	{
		this.isHeadless = isHeadless;
	}
	
	public void setCursorHidden(boolean isHidden)
	{
		if(isHidden)
		{
			executeCommand("ll.oscursor", "0");
		}
		else 
		{
			executeCommand("ll.oscursor", "1");
		}
	}
	
	public Vector2i getSize() 
	{
		return size;
	}

	public String getTitle()
	{
		return title;
	}

	public int getCurrentFPS()
	{
		return currentFPS;
	}

	public void setLogEnabled(boolean enabled)
	{
		logEnabled = enabled;
	}

	public boolean isLogEnabled() 
	{
		return logEnabled;
	}
	
	public void registerLogFormat(int logtype, Vector3f color)
	{
		if(gameConsole == null)
		{
			return;
		}
		gameConsole.registerFormat(logtype, color);
	}
	
	// Game Tool Statics
	public static final int
		TOOL_GAME_CONSOLE 	= 0,
		TOOL_SPRITE_SHEET 	= 1,
		TOOL_FPS_COUNTER 	= 2,
		TOOL_VSYNC			= 3,
		TOOL_OS_CURSOR 		= 4;	
	public static byte[] TOOL_PARAMS = new byte[5];
}
