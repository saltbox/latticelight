package com.saltboxgames.latticelight;

import static org.lwjgl.glfw.GLFW.*;

import java.util.Random;

import org.joml.Vector2f;
import org.joml.Vector2i;
import org.joml.Vector3f;
import com.saltboxgames.latticelight.rendering.*;

public abstract class GameObject 
{
	public static Random spriteRandom = new Random();
	
	protected Vector2i size;
	
	//--Rendering--
	protected Mesh mesh;
	private Vector3f drawPosition;
	protected Vector2f position;
	protected float zDepth;
	
	
	protected boolean hasContext;
	
	/**
	 * @param position Objects Position in world space.
	 * @param size Width and Height of object in tiles.
	 */
	public GameObject(Vector2f position, float zDepth, Vector2i size)
	{	
		this.size = size;
		
		// Init GOLM Variables
		this.position = new Vector2f();
		this.drawPosition = new Vector3f();
		this.zDepth = zDepth;
		
		setPosition(position);
		
		hasContext = (glfwGetCurrentContext() != 0);
		
		if(hasContext)
		{
			mesh = new Mesh(size.x, size.y);	
		}
	}
	/**
	 * @param position Objects Position in world space.
	 * @param size Width and Height of object in tiles.
	 */
	public GameObject(Vector2f position, float zDepth)
	{	
		this(position, zDepth, new Vector2i(1, 1));
	}
	
	public abstract void Update(GameTime gameTime);
	
	public void Draw(SpriteBatch spriteBatch)
	{	
		if(hasContext)
		{
			spriteBatch.Draw(mesh, Camera.Main.getViewProjection().translate(getDrawPosition()));
		}
	}
	
	public void setZDepth(float depth) 
	{
		zDepth = depth;
	}
	
	/**
	 * Set the object's positions.
	 * @param pos Vec2f to set object to.
	 */
	public void setPosition(Vector2f pos)
	{
		this.position.x = pos.x;
		this.position.y = pos.y;
	}
	
	public void setPosition(float x, float y)
	{
		this.position.x = x;
		this.position.y = y;
	}
	
	/**
	 * gets object's position
	 * @param out the Vec2f to output position to.
	 * @return modified input vector
	 */
	public Vector2f getPosition(Vector2f out)
	{	
		out.x = this.position.x;
		out.y = this.position.y;
		return out;
	}
	
	/**
	 * !!!!ONLY USE THIS IF YOU'RE NOT OPPERATING ON THIS OBJECT'S POSITION!!!
	 * @return the objects position
	 */
	public Vector2f getPosition()
	{
		return position;
	}
	
	public Vector3f getDrawPosition() 
	{
		drawPosition.set(position.x, position.y, zDepth);
		return drawPosition;
	}
	
	/**
	 * gets object's size
	 * @param out Vec2f to output size to
	 */
	public Vector2f getSize(Vector2f out) 
	{
		out.x = this.size.x;
		out.y = this.size.y;
		return out;
	}
	
	/**
	 * gets object's size
	 * @param out Vec3f to output size to
	 */
	public Vector2i getSize(Vector2i out) 
	{
		out.x = this.size.x;
		out.y = this.size.y;
		return out;
	}
	
	/**
	 * sets the object's depth for drawing.
	 * @param z
	 */
	public void setDepth(float z)
	{
		this.zDepth = z;
	}
	
	//--------- Tile Setters -
	
	/**
	 * Sets a sprite for a tile on this object.
	 * @param pos [X, Y] position of tile in object, [0, 0] is the top left corner
	 * @param sprite [X, Y] position of sprite in sprite sheet, [0, 0] is the top left corner
	 */
	public void setSprite(Vector2i pos, Vector2i sprite)
	{
		if(hasContext)
		{
			mesh.setQuadTile(pos, sprite);
		}
	}
	
	/**
	 * Sets tile [0, 0]'s sprite
	 * @param sprite XY position of sprite in sprite sheet, 0,0 is the top left corner
	 */
	public void setSprite(Vector2i sprite)
	{
		if(hasContext)
		{
			setSprite(new Vector2i(), sprite);
		}
	}
	
	/**
	 * Sets tile [0, 0]'s sprite
	 * @param by sprite's index location in sprite sheet, 0 being the top left corner.
	 */
	public void setSprite(byte by)
	{
		if(hasContext)
		{
			mesh.setQuadTile(new Vector2i(), by);
		}
	}
	
	/**
	 * @param pos [X, Y] position of tile in object, [0, 0] is the top left corner
	 * @param by sprite's index location in sprite sheet, 0 being the top left corner.
	 */
	public void setSprite(Vector2i startPos, byte by)
	{
		if(hasContext)
		{
			mesh.setQuadTile(startPos, by);
		}
	}
	
	/**
	 * Sets tile [0, 0]'s sprite
	 * @param ch ASCII character to set the sprite to.
	 */
	public void setSprite(char ch)
	{
		if(hasContext)
		{
			setSprite((byte) ch);
		}
	}
	
	/**
	 * @param pos [X, Y] position of tile in object, [0, 0] is the top left corner
	 * @param ch ASCII character to set the sprite to.
	 */
	public void setSprite(Vector2i startPos, char ch)
	{
		if(hasContext)
		{
			mesh.setQuadTile(startPos, (byte) ch);
		}
	}
	
	
	//TODO: This can be optimized by doing only one vbo update.
	/**
	 * Sets sprites randomly
	 * @param sprites
	 */
	public void setAllSpritesRandom(String sprites)
	{
		if(hasContext)
		{
			for (int y = 0; y < size.y; y++) {
				for (int x = 0; x < size.x; x++) {
					mesh.setQuadTile(new Vector2i(x, y), (byte)sprites.charAt(spriteRandom.nextInt(sprites.length())));
				}
			}
		}
	}
	
	//TODO: This can be optimized by doing only one vbo update.
	/**
	 * Sets a series of sprites
	 * @param startPos 
	 * @param msg
	 */
	public void setSpriteText(Vector2i startPos, String msg)
	{	
		if(hasContext)
		{
			setSpriteLine(startPos, msg, new Vector3f(1, 1, 1));
		}
	}
	
	public void setSpriteLine(Vector2i startPos, String msg, Vector3f color)
	{
		if(hasContext)
		{
			for (int i = 0; i < msg.length(); i++) 
			{
				mesh.setQuadTile(new Vector2i(i, 0).add(startPos), (byte) msg.charAt(i));	
				setColor(new Vector2i(i, 0).add(startPos), color);
			}
		}
	}

	public void setColor(Vector2i pos, Vector3f color)
	{
		if(hasContext)
		{
			mesh.setQuadColor(pos, color);
		}
	}
	
	public void setColor(Vector3f color)
	{
		if(hasContext)
		{
			mesh.setQuadColor(new Vector2i(), color);
		}
	}
	
	public void setBackColor(Vector2i pos, Vector3f color)
	{
		if(hasContext)
		{
			mesh.setQuadBackColor(pos, color);
		}
	}
	
	public void setBackColor(Vector3f color)
	{
		if(hasContext)
		{
			mesh.setQuadBackColor(new Vector2i(), color);
		}
	}
	
	//---- Buffer Changes
	public void pushBuffers()
	{
		mesh.pushBuffers();
	}
	
	//-- Sprite
	public void bufSprite(int x, int y, Vector2i sprite)
	{	
		mesh.bufQuadTile(x, y, sprite);
	}
	
	public void bufSprite(Vector2i pos, Vector2i sprite)
	{	
		mesh.bufQuadTile(pos.x, pos.y , sprite);
	}
	
	public void bufSprite(Vector2i sprite)
	{
		bufSprite(0, 0, sprite);
	}
	
	public void bufSprite(int x, int y, byte sprite)
	{	
		mesh.bufQuadTile(x, y, sprite);
	}
	
	public void bufSprite(Vector2i pos, byte by)
	{
		mesh.bufQuadTile(pos.x, pos.y, by);
	}
	
	public void bufSprite(byte by)
	{	
		bufSprite(0, 0, by);
	}
	
	public void bufSprite(int x, int y, char ch)
	{
		bufSprite(x, y, (byte) ch);		
	}
	
	public void bufSprite(Vector2i pos, char ch)
	{
		bufSprite(pos, (byte) ch);		
	}
	
	public void bufSprite(char ch)
	{
		bufSprite((byte) ch);
	}
	
	//-- Foreground Color
	public void bufColor(int x, int y, Vector3f color)
	{
		mesh.bufQuadColor(x, y, color);
	}
	
	public void bufColor(Vector3f color)
	{
		bufColor(0, 0, color);
	}
	
	public void bufColor(int x, int y, Vector3f[] colors)
	{
		mesh.bufQuadColor(x, y, colors);
	}
	
	public void bufColor(Vector3f[] colors)
	{
		mesh.bufQuadColor(0, 0, colors);
	}
	
	//-- Background Color
	public void bufBackColor(int x, int y, Vector3f color)
	{
		mesh.bufQuadBackColor(x, y, color);
	}
	
	public void bufBackColor(Vector3f color)
	{
		bufBackColor(0, 0, color);
	}
	
	public void bufBackColor(int x, int y, Vector3f[] colors)
	{
		mesh.bufQuadBackColor(x, y, colors);
	}
	
	public void bufBackColor(Vector3f[] colors)
	{
		mesh.bufQuadBackColor(0, 0, colors);
	}
}
