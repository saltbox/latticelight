package com.saltboxgames.latticelight;

public class GameTime 
{
	private double deltaTime;
	private double lifeTime;
	
	public double getDeltaTime() 
	{
		return deltaTime;
	}
	
	public double getLifeTime() 
	{
		return lifeTime;
	}
	
	protected void setDeltaTime(double deltaTime) 
	{
		this.deltaTime = deltaTime;
		this.lifeTime += deltaTime;
	}
}
