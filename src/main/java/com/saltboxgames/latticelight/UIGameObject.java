package com.saltboxgames.latticelight;

import org.joml.Vector2f;
import org.joml.Vector2i;

import com.saltboxgames.latticelight.rendering.Camera;
import com.saltboxgames.latticelight.rendering.SpriteBatch;

public abstract class UIGameObject extends GameObject 
{
	public UIGameObject(Vector2f position, float zDepth, Vector2i size) 
	{
		super(position, zDepth, size);
	}

	public UIGameObject(Vector2f position, float zDepth) {
		super(position, zDepth);
	}

	@Override
	public void Draw(SpriteBatch spriteBatch) 
	{
		if(hasContext)
		{
			spriteBatch.Draw(mesh, Camera.Main.getViewProjectionNoTranslation().translate(getDrawPosition()));
		}
	}

}
