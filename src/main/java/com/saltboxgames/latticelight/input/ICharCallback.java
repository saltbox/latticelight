package com.saltboxgames.latticelight.input;

public interface ICharCallback {
	public abstract void charPressed(int keyCode);
}
