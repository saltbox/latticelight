package com.saltboxgames.latticelight.input;

public interface IKeyCallback
{
	abstract void keyCallback(int key, int scancode, int action, int mods);
}
