package com.saltboxgames.latticelight.logging;

public class ConsoleLogger implements ILoggable
{
	public void writeLine(LogMessage msg) 
	{
		switch (msg.getType()) 
		{
		case Logger.LOG_ERROR:
			System.err.println(msg);
			break;
		default:
			System.out.println(msg);
			break;
		}		
	}
}
