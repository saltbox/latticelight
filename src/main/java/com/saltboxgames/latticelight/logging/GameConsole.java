package com.saltboxgames.latticelight.logging;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.joml.Vector2f;
import org.joml.Vector2i;
import org.joml.Vector3f;

import static org.lwjgl.glfw.GLFW.*;

import com.saltboxgames.latticelight.Game;
import com.saltboxgames.latticelight.GameTime;
import com.saltboxgames.latticelight.UIGameObject;
import com.saltboxgames.latticelight.input.ICharCallback;
import com.saltboxgames.latticelight.input.IKeyCallback;

public class GameConsole extends UIGameObject implements ILoggable, IKeyCallback, ICharCallback
{	
	private static int logLenght = 500;
	private static int prevCommandsLenght = 50;
	
	private ArrayList<LogMessage> messageLog;
	private ArrayList<String> prevCommands;
	private Game game;
	
	private int scrollPos = 0;
	private int caretPos = 0;
	private int overwriteMode = 0;
	private int prevCommandIndex = 1;
	
	private boolean doBufLog = true;
	private String inputField = "";
	
	private Map<Integer, Vector3f> messageFormats;
	
	int scollMax = 1;
	
	public GameConsole(Game game, Vector2f position, Vector2i size) 
	{
		super(position, 99.9f, size);
		this.game = game;
		
		messageLog = new ArrayList<LogMessage>();
		prevCommands = new ArrayList<String>();

		messageFormats = new HashMap<>();
		
		messageFormats.put(Logger.LOG_NORMAL, new Vector3f(0.8f, 0.8f, 0.8f));
		messageFormats.put(Logger.LOG_ERROR, new Vector3f(0.9f, 0, 0));
		messageFormats.put(Logger.LOG_STACK_TRACE, new Vector3f(0.6f, 0, 0));
		messageFormats.put(Logger.LOG_DEBUG, new Vector3f(0.95f, 0.95f, 0));
		
		for(int x = 0; x < size.x; x ++)
		{
			bufSprite(x, size.y - 3, new Vector2i(4, 12));
		}
		
		bufScrollBar();	
		bufCaret();
		bufInsertMode();
		bufInputField();
		pushBuffers();
	}
	
	private void bufScrollBar()
	{
		for(int i = 0; i < size.y - 4; i ++)
		{
			bufSprite(size.x - 1, i, (byte)0);
		}
		
		int spritePos = (int) Math.floor(((double)scrollPos / (double)scollMax) * (size.y - 6));
		bufSprite(size.x - 1, size.y - 4, new Vector2i(15, 1));
		bufSprite(size.x - 1, 0, new Vector2i(14, 1));		
		bufSprite(size.x - 1, spritePos + 1, new Vector2i(3, 11));	
	}
	
	private void bufInputField()
	{
		for(int i = 0; i < size.x; i ++)
		{
			if(i < inputField.length())
			{
				bufSprite(i, size.y - 2, inputField.charAt(i));
			}
			else 
			{
				bufSprite(i, size.y - 2, ' ');
			}
		}
	}
	
	private void bufCaret()
	{
		for(int i = 0; i < size.x; i ++)
		{
			bufSprite(i, size.y - 1, new Vector2i(4, 12));
		}
		bufSprite(caretPos, size.y - 1, new Vector2i(15, 13));
	}
	
	private void bufInsertMode()
	{
		if(overwriteMode == 0)
		{
			bufSprite(size.x - 3 , size.y - 3, 'I');
			bufSprite(size.x - 2 , size.y - 3, 'N');
			bufSprite(size.x - 1 , size.y - 3, 'S');
		}
		else 
		{
			bufSprite(size.x - 3 , size.y - 3, 'O');
			bufSprite(size.x - 2 , size.y - 3, 'V');
			bufSprite(size.x - 1 , size.y - 3, 'R');
		}
	}
	
	private void bufLog()
	{
		for (int i = 0; i < size.y - 3; i++)
		{
			if(i + scrollPos < messageLog.size())
			{
				LogMessage msg = messageLog.get(i + scrollPos);
				
				Vector3f color = messageFormats.get(msg.getType());
				if(color == null)
				{
					color = messageFormats.get(Logger.LOG_NORMAL);
				}
				
				for (int x = 0; x < size.x; x++) 
				{
					if(x < msg.length())
					{
						bufColor(x, i, color);
						bufSprite(x, i, msg.charAt(x));
					}
					else
					{
						bufSprite(x, i, ' ');
					}
				}
			}
			else 
			{
				for (int x = 0; x < size.x; x++) 
				{
					bufSprite(x, i, ' ');
				}
			}
		}
	}

	@Override
	public void Update(GameTime gameTime) 
	{	
		if(doBufLog)
		{
			doBufLog = false;
			bufLog();
			bufScrollBar();
		}
		
		pushBuffers();
	}

	@Override
	public void writeLine(LogMessage msg) 
	{
		if(msg != null)
		{
			messageLog.add(msg);
			while(messageLog.size() > logLenght)
			{
				messageLog.remove(0);
			}
			if(messageLog.size() > size.y - 3)
			{
				scrollDown();
			}
			doBufLog = true;
		}
	}
	
	@Override
	public void keyCallback(int key, int scancode, int action, int mods) 
	{
		if(Game.TOOL_PARAMS[Game.TOOL_GAME_CONSOLE] == 1)
		{
			if(action == 1)
			{
				switch (key) 
				{
				case GLFW_KEY_ENTER:
					enter();
					break;
				case GLFW_KEY_INSERT:
					toggleInsertMode();
					break;
				case GLFW_KEY_UP:
					selectPrevCommand();
					break;
				case GLFW_KEY_DOWN:
					selectNextCommand();
					break;
				default:
					break;
				}
			}
			if (action > 0) 
			{
				switch (key) 
				{
				case GLFW_KEY_LEFT:
					backCaret();
					break;
				case GLFW_KEY_RIGHT:
					forwardCaret();
					break;
				case GLFW_KEY_BACKSPACE:
					if(inputField.length() > 0)
					{
						backSpace();
					}
					break;
				case GLFW_KEY_PAGE_UP:
					scrollUp();
					break;
				case GLFW_KEY_PAGE_DOWN:
					scrollDown();
					break;
				default:
					break;
				}
			}
		}
	}

	@Override
	public void charPressed(int asciiCode) 
	{
		if(Game.TOOL_PARAMS[Game.TOOL_GAME_CONSOLE] == 1)
		{
			if(asciiCode != GLFW_KEY_GRAVE_ACCENT && asciiCode != 126)
			{
				keyPress(asciiCode);
			}
		}
	}
	
	private void backSpace()
	{
		if(caretPos > 0)
		{
			if(caretPos != inputField.length())
			{
				inputField = inputField.substring(0, caretPos - 1) + inputField.substring(caretPos, inputField.length());
			}
			else 
			{
				inputField = inputField.substring(0, caretPos - 1);
			}
			backCaret();
			bufInputField();
		}
	}
	
	private void enter()
	{
		if(inputField.length() > 0)
		{
			executeCommand();
		}
	}
	
	private void keyPress(int asciiCode)
	{
		int offset = caretPos + overwriteMode;
		if(offset > inputField.length())
		{
			offset -= 1;
		}
		inputField = inputField.substring(0, caretPos) + (char) asciiCode + inputField.substring(offset, inputField.length());

		forwardCaret();
		bufInputField();
	}
	
	private void resetCaret()
	{
		caretPos = 0;
		bufCaret();
	}
	
	private void forwardCaret()
	{
		caretPos ++;
		if(caretPos > inputField.length())
		{
			caretPos = inputField.length();
		}
		
		bufCaret();
	}
	
	private void backCaret()
	{
		caretPos --;
		if(caretPos < 0)
		{
			caretPos = 0;
		}
		bufCaret();
	}
	
	private void toggleInsertMode()
	{
		if(overwriteMode == 0)
		{
			overwriteMode = 1;
		}
		else 
		{
			overwriteMode = 0;
		}
		bufInsertMode();
	}
	
	private void scrollUp()
	{
		scrollPos --;
		if(scrollPos < 0)
		{
			scrollPos = 0;
		}
		doBufLog = true;
	}
	
	private void scrollDown()
	{
		scrollPos ++;
		if((size.y - 3) + scrollPos > messageLog.size())
		{
			scrollPos--;
		}
		if(scrollPos > scollMax)
		{
			scollMax ++;
		}	
		doBufLog = true;
	}
	
	private void selectPrevCommand()
	{
		if(prevCommands.size() > 0)
		{
			prevCommandIndex --;
			if(prevCommandIndex < 0)
			{
				prevCommandIndex = 0;
			}
			inputField = prevCommands.get(prevCommandIndex);
			caretPos = inputField.length();
			
			bufCaret();
			bufInputField();
		}
	}
	
	private void selectNextCommand()
	{
		prevCommandIndex ++;
		if(prevCommandIndex >= prevCommands.size())
		{
			prevCommandIndex = prevCommands.size();
			inputField = "";
		}
		else 
		{
			inputField = prevCommands.get(prevCommandIndex);
		}	
		caretPos = inputField.length();
		
		bufCaret();
		bufInputField();
	}
	
	private void executeCommand() 
	{
		sendMessage(inputField);
		ArrayList<String> splitString = new ArrayList<String>(Arrays.asList(inputField.split(" +")));
		String[] args = null;
		if(splitString.size() > 1)
		{
		    for (int i = 0; i < splitString.size(); i++)
		    {
		        if(enclosedInQuotes(splitString.get(i)))
		        {
		            splitString.set(i, splitString.get(i).substring(1, splitString.get(i).length() - 1));
		        }
		        if (hasOnlyOneQuote(splitString.get(i)))
		        {
		            while (true)
		            {
		                if (i + 1 >= splitString.size())
		                {
		                    Logger.writeLine("Mismatched quotes");
		                    return;
		                }
		                splitString.set(i, splitString.get(i) + " " + splitString.get(i + 1));
		                splitString.remove(i + 1);
		                 
		                if (enclosedInQuotes(splitString.get(i)))
		                {
		                    splitString.set(i, splitString.get(i).substring(1, splitString.get(i).length() - 1));
		                    break;
		                }
		            }
		        }
		    }
		    args = new String[splitString.size() - 1];
		    System.arraycopy(splitString.toArray(), 1, args, 0, splitString.size() - 1);
		}
   
		if(args == null)
		{
			args = new String[0];
		}
		
		if(!game.executeCommand(splitString.get(0), args))
		{
		   Logger.writeLine("\"" + splitString.get(0) + "\" not found!", Logger.LOG_ERROR);
		}
		
		inputField = "";
		resetCaret();
		bufInputField();
	}
	
	private boolean hasOnlyOneQuote(String s) {
        //match for a word with a ' or " before or after it, but not both
        return s.matches("[\'\"]\\w+|\\w+[\'\"]");
    }
	
	private boolean enclosedInQuotes(String s) {
        //match for a string enclosed in 's or "s
        return s.matches("(\".*\")|(\'.*\')");
    }
	
	private void sendMessage(String msg)
	{
		prevCommands.add(msg);
		if(prevCommands.size() > prevCommandsLenght)
		{
			prevCommands.remove(0);
		}
		prevCommandIndex = prevCommands.size();
		writeLine(new LogMessage(msg, Logger.LOG_NORMAL));
	}
	
	public void registerFormat(int logtype, Vector3f color)
	{
		messageFormats.put(logtype, color);
	}
}
