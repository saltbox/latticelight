package com.saltboxgames.latticelight.logging;

public class LogMessage 
{
	private String value;
	private int type;
	
	public LogMessage(String value, int type)
	{
		this.value = value;
		this.type = type;
	}
	
	public int getType() {
		return type;
	}
	
	public String getValue() {
		return value;
	}
	
	public int length()
	{
		return value.length();
	}
	
	public char charAt(int index)
	{
		return value.charAt(index);
	}
	
	@Override
	public String toString() 
	{
		return value;
	}
}
