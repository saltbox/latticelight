package com.saltboxgames.latticelight.logging;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class Logger 
{
	private static int back_log_size = 100;
	private static List<ILoggable> logs = new ArrayList<ILoggable>();
	private static Queue<LogMessage> back_log = new LinkedList<LogMessage>();
	
	private static long log_mode;
	
	public static final int 
		LOG_NORMAL 		= 0,
		LOG_ERROR		= 1,
		LOG_STACK_TRACE	= 2,
		LOG_DEBUG 		= 3;
	
	
	static // static initializers are nice
	{
		enableLogType(LOG_NORMAL, LOG_ERROR);
	}
	
	public static void enableLogType(int... logtype)
	{
		for (int i = 0; i < logtype.length; i++) 
		{
			log_mode |= (1 << logtype[i]);
		}
	}
	
	public static void disableLogType(int... logtype)
	{
		for (int i = 0; i < logtype.length; i++) 
		{
			log_mode &= ~(1 << logtype[i]);
		}
	}
	
	public static void registerLog(ILoggable iLoggable)
	{
		if(!logs.contains(iLoggable))
		{
			logs.add(iLoggable);
			for (LogMessage message : back_log) 
			{	
				iLoggable.writeLine(message);	
			}
		}
	}
	
	public static void writeLine(String msg, int logType)
	{
		if ((log_mode & (1L << logType)) != 0)
		{
			LogMessage message = new LogMessage(msg, logType);
			back_log.add(message);
			while(back_log.size() > back_log_size)
			{
				back_log.remove();
			}
			for (ILoggable iLoggable : logs) 
			{
				iLoggable.writeLine(message);
			}
		}
	}
	
	public static void writeLine(Object obj, int logtype)
	{
		writeLine(obj.toString(), logtype);
	}
		
	public static void writeLine(String msg)
	{
		writeLine(msg, LOG_NORMAL);
	}
	
	public static void writeLine(Object obj)
	{
		writeLine(obj.toString(), LOG_NORMAL);
	}
	
	
	
	// Exception handling
	static StringWriter sw = new StringWriter();
	static PrintWriter pw = new PrintWriter(sw);
	public static void writeLine(Exception e)
	{
		if(e.getMessage() != null)
		{
			writeLine(e.getMessage(), LOG_ERROR);
		}	
		if ((log_mode & (1L << LOG_STACK_TRACE)) != 0)
		{
			e.printStackTrace(pw);
			String stline = sw.toString();
			String[] stackTrace = stline.split("\n");
			
			for(int i = 0; i < stackTrace.length; i ++)
			{
				writeLine("  " + stackTrace[i].replaceAll("[\r\t]", ""), LOG_STACK_TRACE);
			}
			
			pw.flush();
			sw.getBuffer().setLength(0);
		}
	}
}
