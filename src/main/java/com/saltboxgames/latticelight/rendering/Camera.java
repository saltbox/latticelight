package com.saltboxgames.latticelight.rendering;

import org.joml.Matrix4f;
import org.joml.Vector2f;
import org.joml.Vector3f;

public class Camera 
{
	public static Camera Main;
	
	//-- View
	private Vector3f position;
	private Matrix4f perspective;
	private float width, height, scale;
	private float zNear = -100, zFar = 100;
	
	
	public Camera(Vector3f pos, float width, float height, float scale)
	{	
		perspective = new Matrix4f();
		
		this.height = height;
		this.width = width;
		this.scale = scale;		
	//	setOrthographic(width, height, scale);
		position = pos;
	}
	
	/*
    void setOrthographic(float width, float height, float scale)
	{
    	//perspective.setOrtho2D(0, (width * scale), (height * scale), 0);
    	perspective.setOrtho(0, (width * scale), (height * scale), 0, -100, 100);
	}
    */
	
	/**
	 * 
	 * @return
	 */
    public Matrix4f getViewProjection()
    {
    	return perspective.identity().setOrtho(0, (width * scale), (height * scale), 0, zNear, zFar).translate(position);
    //	return perspective;
    }
    
    public Matrix4f getViewProjectionNoTranslation()
    {
    	return perspective.identity().setOrtho(0, (width * scale), (height * scale), 0, zNear, zFar);
    }
    
    public void setZFar(float zFar) 
    {
		this.zFar = zFar;
	}
    
    public void setZNear(float zNear) 
    {
		this.zNear = zNear;
	}
    
    /**
     * Sets the camera's position in world space.
     * @param position to set the camera too.
     */
    public void setPosition(Vector3f position) 
    {
    	this.position.x = -position.x;
		this.position.y = -position.y;	
		this.position.z = -position.z;
	}
    
    /**
     * Sets the camera's position in world space.
     * @param position to set the camera too.
     */
    public void setPosition(Vector2f position) 
    {
    	this.position.x = -position.x;
		this.position.y = -position.y;	
	}
    
    /**
     * gets the position of the Camera in world space
     * @param out position of the camera
     * @return 
     */
    public Vector2f getPosition(Vector2f out)
    {
    	out.x = -this.position.x;
    	out.y = -this.position.y;
    	return out;
    }
    
    /**
     * gets the position of the Camera in world space
     * @param out position of the camera
     */
    public void getPosition(Vector3f out)
    {
    	out.x = -this.position.x;
    	out.y = -this.position.y;
    	out.y = -this.position.z;
    }
}
