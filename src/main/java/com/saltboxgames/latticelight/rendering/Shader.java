package com.saltboxgames.latticelight.rendering;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL20.*;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.FloatBuffer;

import org.joml.Matrix4f;
import org.joml.Vector3f;
import org.lwjgl.BufferUtils;

import com.saltboxgames.latticelight.logging.Logger;

public class Shader 
{	
	private static final int UNI_TRANSFORM = 0;
	private static final int UNI_TEXTURE1 = 1;
	
	protected int programID;
	private int vertexShaderID, fragmentShaderID;
	
	private int[] typical_uniforms;
	
	public Shader(String vertPath, String fragPath)
	{
		vertexShaderID = loadShader(vertPath, GL_VERTEX_SHADER);
		fragmentShaderID = loadShader(fragPath, GL_FRAGMENT_SHADER);
		
		programID = glCreateProgram();
		glAttachShader(programID, vertexShaderID);
		glAttachShader(programID, fragmentShaderID);
		
		glBindAttribLocation(programID, 0, "in_position"); 
        glBindAttribLocation(programID, 1, "in_color"); 
        glBindAttribLocation(programID, 2, "in_texCoord0");
		
		glLinkProgram(programID);
		glValidateProgram(programID);
		
		//Cache typical uniform variables
		typical_uniforms = new int[]
		{
			glGetUniformLocation(programID, "transform"), 	// UNI_TRANSFORM = 0;
			glGetUniformLocation(programID, "texture1")		//  UNI_TEXTURE1 = 1;
		};
		
	}
	
	public int getUniform(String name)
	{
		int result = glGetUniformLocation(programID, name);
		if(result == -1)
		{
			Logger.writeLine("Could not find uniform: " + name + " in shader program: " + programID, Logger.LOG_ERROR);
		}
		return result;
	}
	
	public void setUniform_v3f(String name, Vector3f vec3)
	{
		glUniform3f(getUniform(name), vec3.x, vec3.y, vec3.z);
	}
	
	private FloatBuffer matrixfb = BufferUtils.createFloatBuffer(16);
	public void Update(Matrix4f transform)
	{
		transform.get(matrixfb);
		
		glUniformMatrix4fv(typical_uniforms[UNI_TRANSFORM], false, matrixfb);
		matrixfb.clear();
	}
	
	public void setTexture1(int TextureNum)
	{
		glUniform1i(typical_uniforms[UNI_TEXTURE1], TextureNum);
	}
	
	public void bind()
	{
		glUseProgram(programID);
	}
	
	public void unbind()
	{
		glUseProgram(0);
	}
	
	private static int loadShader(String path, int type)
	{
		StringBuilder result = new StringBuilder();
		try 
		{
			BufferedReader reader = new BufferedReader(new FileReader(path));
			String line = "";
			while ((line = reader.readLine()) != null)
			{
				result.append(line);
				result.append('\n');
			}
			reader.close();
		} 
		catch (IOException e) 
		{
			// TODO: SHADER FILE READ ERROR
			Logger.writeLine(e);
			return -1;
		}
		
		int shaderID = glCreateShader(type);
		glShaderSource(shaderID, result.toString());
		glCompileShader(shaderID);
		if(glGetShaderi(shaderID, GL_COMPILE_STATUS) == GL_FALSE)
		{
			//TODO: ERROR COMPILING SHADER
			System.err.println(glGetShaderInfoLog(shaderID, 500));
			System.err.println("Could not compile shader");
			return -1;
		}
		
		return shaderID;
	}
}
