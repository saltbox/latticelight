package com.saltboxgames.latticelight.rendering;

import org.joml.Matrix4f;

import com.saltboxgames.latticelight.logging.Logger;

public class SpriteBatch 
{
	private Shader activeShader;
	private Texture activeTexture;
	
	public SpriteBatch() 
	{
	}
	
	public void Draw(Mesh mesh, Matrix4f transform)
	{
		activeShader.Update(transform);
		mesh.Draw();
	}
	
	public void setTexture(Texture texture)
	{
		if(activeTexture != texture)
		{
			activeTexture = texture;
			texture.bind(0); // <-- TODO: this really needs to get fixed;
		}
	}
	
	public void setShader(Shader shader)
	{
		if(activeShader != shader)
		{
			activeShader = shader;
			shader.bind();
		}
	}
}
