package com.saltboxgames.latticelight.rendering;

import java.nio.ByteBuffer;
import java.nio.IntBuffer;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL13;

import com.saltboxgames.latticelight.logging.Logger;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.stb.STBImage.*;

public class Texture 
{
	protected int id;
    private int width;
    private int height;

	public Texture(int width, int height, ByteBuffer data) 
	{
		buildTexture(width, height, data);
	}
	
	public Texture(String path)
	{
		IntBuffer w = BufferUtils.createIntBuffer(1);
		IntBuffer h = BufferUtils.createIntBuffer(1);
		IntBuffer comp = BufferUtils.createIntBuffer(1);
		
		//load image
		stbi_set_flip_vertically_on_load(1);
		ByteBuffer image = stbi_load(path, w, h, comp, 4);
		if (image == null)
		{
			Logger.writeLine("Failed to load image: " + path, Logger.LOG_ERROR);
		}
		
		/* Get width and height of image */
	    int width = w.get();
	    int height = h.get();
	    
	    buildTexture(width, height, image);
	}
	
	public void bind(int TextureNum)
	{
		GL13.glActiveTexture(GL13.GL_TEXTURE0 + TextureNum);
		glBindTexture(GL_TEXTURE_2D, id);
	}
	
	private void buildTexture(int width, int height, ByteBuffer data)
	{
		id = glGenTextures();
	    this.width = width;
	    this.height = height;

		glBindTexture(GL_TEXTURE_2D, id);
		
	    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	
	    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
	}
	
	public int getHeight() {
		return height;
	}
	public int getWidth() {
		return width;
	}
}
