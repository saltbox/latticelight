package com.saltboxgames.latticelight.rendering;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL30.*;
import static com.saltboxgames.latticelight.utils.Utilities.*;

import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

import com.saltboxgames.latticelight.logging.Logger;

public class VertexBufferObject 
{
	public static final int 	VERTEX_ATTRIB = 0;
	public static final int  	 COLOR_ATTRIB = 1;
	public static final int 	TCOORD_ATTRIB = 2;
	public static final int BACK_COLOR_ATTRIB = 3;
	
	private int count;
	private int id;
	private int vbo_id, tbo_id, cbo_id, ibo_id, bcbo_id; 
	
	public VertexBufferObject(float[] vertices, float[] texcoords, float[] colors, float[] backgroundColors, short[] indices)
	{
		id = glGenVertexArrays();
		glBindVertexArray(id);
		
		buildVertexBuffer(createFloatBuffer(vertices));
		buildColorBuffer(createFloatBuffer(colors));
		buildTexCoordBuffer(createFloatBuffer(texcoords));
		buildBackgroundColorBuffer(createFloatBuffer(backgroundColors));
		buildIndicesBuffer(createShortBuffer(indices));
		
		count = indices.length;
		
		glBindVertexArray(0);
	}
	
	public VertexBufferObject(FloatBuffer vertices, FloatBuffer texcoords, FloatBuffer colors, FloatBuffer backgroundColors, ShortBuffer indices)
	{
		id = glGenVertexArrays();
		glBindVertexArray(id);
		
		buildVertexBuffer(vertices);
		buildColorBuffer(colors);
		buildTexCoordBuffer(texcoords);
		buildBackgroundColorBuffer(backgroundColors);
		buildIndicesBuffer(indices);
		
		count = indices.capacity();
		
		glBindVertexArray(0);
	}
	
	public void Draw()
	{
		glBindVertexArray(id);
		//Enable Attributes
		glEnableVertexAttribArray(VERTEX_ATTRIB);
		glEnableVertexAttribArray(COLOR_ATTRIB);
		glEnableVertexAttribArray(TCOORD_ATTRIB);
		glEnableVertexAttribArray(BACK_COLOR_ATTRIB);
			
		glDrawElements(GL_TRIANGLES, count, GL_UNSIGNED_SHORT, 0);
		
		glDisableVertexAttribArray(VERTEX_ATTRIB);
		glDisableVertexAttribArray(COLOR_ATTRIB);
		glDisableVertexAttribArray(TCOORD_ATTRIB);
		glDisableVertexAttribArray(BACK_COLOR_ATTRIB);
		
		glBindVertexArray(0);
	}
	
	private void buildVertexBuffer(FloatBuffer vertices)
	{		
		vbo_id = glGenBuffers();

		glBindBuffer(GL_ARRAY_BUFFER, vbo_id);
		glBufferData(GL_ARRAY_BUFFER, vertices, GL_STATIC_DRAW);	
		glVertexAttribPointer(VERTEX_ATTRIB, 3, GL_FLOAT, false, 0, 0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}
	
	private void buildTexCoordBuffer(FloatBuffer texcoords)
	{
		tbo_id = glGenBuffers();

		glBindBuffer(GL_ARRAY_BUFFER, tbo_id);
		glBufferData(GL_ARRAY_BUFFER, texcoords, GL_DYNAMIC_DRAW);	
		glVertexAttribPointer(TCOORD_ATTRIB, 2, GL_FLOAT, false, 0, 0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}
	
	private void buildColorBuffer(FloatBuffer colors)
	{
		cbo_id = glGenBuffers();

		glBindBuffer(GL_ARRAY_BUFFER, cbo_id);
		glBufferData(GL_ARRAY_BUFFER, colors, GL_DYNAMIC_DRAW);	
		glVertexAttribPointer(COLOR_ATTRIB, 3, GL_FLOAT, false, 0, 0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}
	
	private void buildBackgroundColorBuffer(FloatBuffer colors)
	{
		bcbo_id = glGenBuffers();

		glBindBuffer(GL_ARRAY_BUFFER, bcbo_id);
		glBufferData(GL_ARRAY_BUFFER, colors, GL_DYNAMIC_DRAW);	
		glVertexAttribPointer(BACK_COLOR_ATTRIB, 3, GL_FLOAT, false, 0, 0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}
	
	private void buildIndicesBuffer(ShortBuffer indices)
	{
		ibo_id = glGenBuffers();

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo_id);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices, GL_STATIC_DRAW);	
	}
	
	public void updateTexCoords(int start, FloatBuffer ntexCoords)
	{
		glBindBuffer(GL_ARRAY_BUFFER, tbo_id);
		glBufferSubData(GL_ARRAY_BUFFER, start * 4, ntexCoords);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}
	
	public void updateColor(int start, FloatBuffer nColor)
	{
		glBindBuffer(GL_ARRAY_BUFFER, cbo_id);
		glBufferSubData(GL_ARRAY_BUFFER, start * 4, nColor);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}
	
	public void updateBackColor(int start, FloatBuffer nColor)
	{
		glBindBuffer(GL_ARRAY_BUFFER, bcbo_id);
		glBufferSubData(GL_ARRAY_BUFFER, start * 4, nColor);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}
	
	public int getId() 
	{
		return id;
	}
	
	@Override
	protected void finalize() throws Throwable {
		
		//Clean up OpenGL
		glDeleteBuffers(vbo_id);
		glDeleteBuffers(tbo_id);
		glDeleteBuffers(cbo_id);
		glDeleteBuffers(ibo_id);
		glDeleteBuffers(bcbo_id);
		glDeleteVertexArrays(id);
		
		Logger.writeLine("Deleted:  " + id);
		
		super.finalize();
	}
}
