package com.saltboxgames.latticelight.utils;

@FunctionalInterface
public interface Command 
{
	public void run(String[] args);
}
