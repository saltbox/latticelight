package com.saltboxgames.latticelight.utils;

import org.joml.Vector2f;
import org.joml.Vector2i;

import com.saltboxgames.latticelight.Game;
import com.saltboxgames.latticelight.GameTime;
import com.saltboxgames.latticelight.UIGameObject;

public class FPSCounter extends UIGameObject 
{
	private float updateTime = 0;
	private Game game;
	
	public FPSCounter(Vector2f position, float zDepth, Game game) 
	{
		super(position, zDepth, new Vector2i(12, 1));
		this.game = game;
		setSpriteText(new Vector2i(), "FPS         ");
	}
	
	@Override
	public void Update(GameTime gameTime)
	{		
		updateTime += gameTime.getDeltaTime();
		if(updateTime >- 1)
		{
			setSpriteText(new Vector2i(4, 0), "" + game.getCurrentFPS() + "       ");
			updateTime -= 1;
		}	
	}
}
