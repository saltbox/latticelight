package com.saltboxgames.latticelight.utils;

import org.joml.Vector2f;
import org.joml.Vector2i;
import org.joml.Vector3f;

import com.saltboxgames.latticelight.GameTime;
import com.saltboxgames.latticelight.UIGameObject;
import com.saltboxgames.latticelight.input.MouseInput;
import static org.lwjgl.glfw.GLFW.*;

public class SpritesheetTester extends UIGameObject
{
	static final int TILE_COUNT = 16;
	Vector2f mousePosition = new Vector2f();
	Vector2i cursorReadoutPosition;
	Vector3f readoutColor = new Vector3f(0f, 1f, 0f);
	float tileSize;
	
	public SpritesheetTester(float tileSize)
	{
		super(new Vector2f(0, 0), 99f, new Vector2i(TILE_COUNT, TILE_COUNT + 1));
		cursorReadoutPosition = new Vector2i(0, TILE_COUNT);
		Vector2i v = new Vector2i();
		this.tileSize = tileSize;
		for (v.x = 0; v.x < TILE_COUNT; v.x++) {
			for (v.y = 0; v.y < TILE_COUNT; v.y++) {
				bufSprite(v, v);
			}
		}
		pushBuffers();
	}
		
	@Override
	public void Update(GameTime gameTime)
	{
		MouseInput.getPos(mousePosition);
		setSpriteLine(cursorReadoutPosition, Math.floor(mousePosition.x / tileSize) + " " + Math.floor(mousePosition.y / tileSize) + "                ", readoutColor);
	}
}
