package com.saltboxgames.latticelight.utils;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;

import org.lwjgl.BufferUtils;

public class Utilities 
{
	public static boolean canParseInt(String str)
	{
		return str.matches("^-?\\d+$");
	}
	
	// Buffer Utils
	public static FloatBuffer createFloatBuffer(float[] data)
	{
		FloatBuffer buffer = BufferUtils.createFloatBuffer(data.length);
		buffer.put(data);
		buffer.flip();
		return buffer;
	}
	
	public static ByteBuffer createByteBuffer(byte[] data)
	{
		ByteBuffer buffer = BufferUtils.createByteBuffer(data.length);
		buffer.put(data);
		buffer.flip();
		return buffer;
	}
	
	public static ShortBuffer createShortBuffer(short[] data)
	{
		ShortBuffer buffer = BufferUtils.createShortBuffer(data.length);
		buffer.put(data);
		buffer.flip();
		return buffer;
	}
	
	public static IntBuffer createIntBuffer(int[] data)
	{
		IntBuffer buffer = BufferUtils.createIntBuffer(data.length);
		buffer.put(data);
		buffer.flip();
		return buffer;
	}
	
	//This is dirty, but it's faster for what we're doing.
	public static byte[] stringToBytesASCII(String str) 
	{
		 byte[] buffer = new byte[str.length()];
		 for (int i = 0; i < buffer.length; i++) 
		 {
			 buffer[i] = (byte) str.charAt(i);
		 }
		 return buffer;
	}
}
