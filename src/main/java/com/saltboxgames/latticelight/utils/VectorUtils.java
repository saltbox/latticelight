package com.saltboxgames.latticelight.utils;

import org.joml.Vector2f;
import org.joml.Vector3f;

public class VectorUtils 
{
	
	/**
	 * Copys vector3f values into a vector2f, the Z axis is dropped.
	 * @param in Vector3f to copy values from
	 * @param out Vector2f to copy values to
	 */
	public static void Vec3fToVec2f(Vector3f in, Vector2f out)
	{
		out.x = in.x;
		out.y = in.y;
	}
	
	/**
	 * Copys vector2f values into a vector3f
	 * @param in vector2f to copy values from
	 * @param out vector3f to copy values to
	 */
	public static void Vec2fToVec3f(Vector2f in, Vector3f out)
	{
		out.x = in.x;
		out.y = in.y;
		out.z = 0;
	}
}
